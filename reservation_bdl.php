<?php 
include "connect/connect.php";

if(isset($_SESSION['membre']))	 
	{
	  $requser = $bdd->prepare('SELECT * FROM membres WHERE id = ?');
      $requser->execute(array($_SESSION['membre']));
      $userinfo = $requser->fetch();

	}else{
		header ("Location: connexion.php");
	}

	
	if(isset($_POST['submit'])){
		
		$appareil = htmlspecialchars($_POST['appareil']);
		$date = htmlspecialchars($_POST['date']);
		$dure = htmlspecialchars($_POST['dure']);
		$dispo = htmlspecialchars($_POST['dispo']);
		
		if(!empty($appareil) and (!empty($date)) and (!empty($dure)) and (!empty($dispo))){
		
			$insertcours = $bdd->prepare("INSERT INTO reservations VALUES(?,?,?,?,?,?,?,NOW(),?)");
			$insertcours->execute(array(NULL,$userinfo['id'], $appareil, "bdl", $date, $dure, $dispo,0));
			
			$error = "Votre réservation a bien été enregistrée, vous recevrez une confirmation par mail !";
			
		}else{
			$error = "&#9888; Tous les champs doivent être complétés !";
		}
		
		
	}
	
?>

<div class="container">
<?php include'body/header.php'; ?>

<div id="crps">

	<h4>Réservation d'un Baptême de l'air</h4><br>
<table class="table">

  <thead>
	 <tr>
      <th scope="col">Appareil</th>
      <th scope="col">Tarif</th>
    </tr>
  </thead>
  <tbody>
    <tr id="colchang">
      <td>Pendulaire</td>
	  <td>190.00 €</td>
    </tr>
    <tr>
      <td>Autogire</td>
	  <td>260.00 €</td>
    </tr>
    <tr id="colchang">
      <td>Multiaxes</td>
	  <td>380.00 €</td>
    </tr>
	<tr><td></td></tr>
  </tbody>
</table>
	
<div id="conlist">
<p id="error"><?php echo (isset($error)) ? $error : ''; ?></p>
	<form method="POST">
	  <div class="form-group">
		<label for="exampleFormControlSelect1">Appareil</label><br>
		<select class="form-control" id="exampleFormControlInput1" name="appareil">
			<option value="Pendulaire">Pendulaire</option>
			<option value="Autogire">Autogire</option>
			<option value="Multiaxes">Multiaxes</option>
		</select>
	  </div>
	  <div class="form-group">
		<label for="exampleFormControlSelect2">Date</label>
		<input type="date" class="form-control" id="exampleFormControlInput1" name="date">
	  </div>
	   <div class="form-group">
		<label for="exampleFormControlSelect2">Durée en H</label>
		<input type="number" class="form-control" id="exampleFormControlInput1" name="dure">
	  </div>
	   <div class="form-group">
		<label for="exampleFormControlSelect2">Disponibilité</label>
		<input type="text" placeholder="Ex : Entre 13h et 15h" class="form-control" id="exampleFormControlInput1" name="dispo">
	  </div>
	  
	<div id="divbtn">	 
		<button id="button" type="submit" name="submit" >valider</button>
	</div>
	</form>
 </div>	
 
</div>

<?php include'body/footer.html'; ?>
</div>

