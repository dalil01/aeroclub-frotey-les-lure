﻿<?php 
include "connect/connect.php";
?>
<div class="container">
<?php include'body/header.php'; ?>

<div id="crps">
<h4>Nos tarifs 2019-2020</h4>
<h5>Baptêmes de l'air/initiation</h5>

<div id="mysped">
<div class="container" >
  <div class="row">
    <div class="col-sm-5">
      <h6 style="color: #000; margin-top: -15px;">Baptême de l'air ULM Pendulaire</h6><br>
      <p><img id="moyimg" src="images/bb1.png" ></p>
   </div>
    <div class="col-sm-7">
	 <h6 style="color: #000; margin-top: 5px;">Prestation :</h6>
      <p>Accompagné par un instructeur diplômé, vous découvrez le vol en ULM. 
	  Survol de la région : découvrez les paysages superbes de Moffans-et-Vacheresse,
	  Lyoffans et Roye. 20 minutes de vol dans une ambiance détendue et conviviale.</p>
	  <h6 style="text-align: center; margin-top: 5px;">Tarif : 190.00 €</h6>
    </div>
  </div>
  <br> 
  
   <div class="row">
    <div class="col-sm-5">
      <h6 style="color: #000; margin-top: -15px;">Baptême de l'air ULM AutoGire</h6><br>
      <p><img id="moyimg" src="images/tari2.jpg" ></p>
   </div>
    <div class="col-sm-7">
	 <h6 style="color: #000; margin-top: 5px;">Prestation :</h6>
      <p>Vous prendrez pour la première fois les commandes d'un ULM à bord d'un 
	  appareil dont la simplicité vous étonnera. Un instructeur diplômé vous
	  accompagnera tout au long de ce vol d'initiation. Le système de 
	  double commande vous permettra en toute sécurité de découvrir le 
	  pilotage d'un appareil hors normes. Pendant 30 minutes,
	  initiez-vous au maniement des commandes de base de cet appareil et 
	  découvrez les sensation d'un vol à bord d'un ULM.</p>
	  <h6 style="text-align: center; margin-top: 5px;">Tarif : 260.00 €</h6>
    </div>
  </div>
  <br>
  
   <div class="row">
    <div class="col-sm-5">
      <h6 style="color: #000; margin-top: -15px;">Baptême de l'air ULM multiaxes</h6><br>
      <p><img id="moyimg" src="images/tari3.jpg" ></p>
   </div>
    <div class="col-sm-7">
	 <h6 style="color: #000; margin-top: 5px;">Prestation :</h6>
      <p>Prenez place à bord d'un ULM dont le comportement vous étonnera par
	  sa ressemblance avec celui d'un avion. Un instructeur diplômé vous 
	  accompagnera tout au long de ce vol d'initiation. Découvrez en toute 
	  sécurité le pilotage. Pendant 30 minutes,
	  initiez-vous au maniement des commandes de base de cet appareil.</p>
	  <h6 style="text-align: center; margin-top: 5px;">Tarif : 380.00 €</h6>
    </div>
  </div>
</div>
</div>
<h5>Location d'appareil</h5>
<table class="table">
  <thead>
	 <tr>
      <th scope="col">Appareil</th>
      <th scope="col">Heure de vol</th>
      <th scope="col">Forfait 40 heures</th>
	  <th scope="col">Forfait annuel illimité</th>
    </tr>
  </thead>
  <tbody>
    <tr id="colchang">
      <td>Air Creation Trek HKS 700E FUN450</td>
	  <td>80.00 €</td>
	  <td>2500.00 €</td> 
	  <td>5200.00 €</td> 
    </tr>
    <tr>
      <td>MAGNI M22 - Rotax 914</td>
	  <td>120.00 €</td>
	  <td>4000.00 €</td> 
	  <td>8100.00 €</td>
    </tr>
    <tr id="colchang">
      <td>Croses Criquet léger LC 12</td>
	  <td>90.00 €</td>
	  <td>2900.00 €</td> 
	  <td>6000.00 €</td>
    </tr>
	<tr><td></td></tr>
  </tbody>
</table>

<h5>Cotisation annuelle Club :</h5>
<p> Club 150,00 € (Licence FFPlum ou FFV exigée).</p>
<h5>Formation pilotage  :</h5>
<p>Nous contacter : acf2l@gmail.com </p>
</div>

<?php include'body/footer.html'; ?>
</div>