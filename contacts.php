﻿<?php
include "connect/connect.php";

if(isset($_POST['submit'])){
	$nom = htmlspecialchars(ucfirst(trim($_POST['nom'])));
	$mail = htmlspecialchars(trim(strtolower($_POST['mail'])));
	$sujet = htmlspecialchars(ucfirst(trim($_POST['sujet'])));
	$message = htmlspecialchars(ucfirst(trim($_POST['message'])));
	
	if(!empty($nom) and (!empty($mail)) and (!empty($sujet)) and (!empty($message))){
		
		

		$fp = fopen("contacts/$nom.txt","w+");

		fputs($fp,"Nom : $nom
Adresse mail : $mail
Sujet : $sujet
Message : $message");
		
		fclose($fp);

		$error = "Votre message a bien été envoyé !";
		
	}else{
		$error = "&#9888; Tous les champs doivent être complétés !";
	}
}

?>
<div class="container">
<?php include'body/header.php'; ?>

<div id="crps">
<h4>Nous contacter</h4>
<h5>Contact</h5>

<div id="contactlist">
<p id="error"><?php echo (isset($error)) ? $error : ''; ?></p>
	<form method="POST">
	  <div class="form-group">
		<label for="exampleFormControlInput1">Votre nom</label>
		<input type="text" name="nom" class="form-control" id="exampleFormControlInput1" value="<?php if(isset($nom)) { echo $nom; } ?>">
	  </div>
	  <div class="form-group">
		<label for="exampleFormControlSelect1">Adresse mail</label>
		<input type="email" name="mail" class="form-control" id="exampleFormControlInput1" value="<?php if(isset($mail)) { echo $mail; } ?>">
	  </div>
	  <div class="form-group">
		<label for="exampleFormControlSelect2">Sujet</label>
		<input type="text" name="sujet" class="form-control" id="exampleFormControlInput1" value="<?php if(isset($sujet)) { echo $sujet; } ?>">
	  </div>
	  <div class="form-group">
		<label for="exampleFormControlTextarea1">Message</label>
		<textarea class="form-control" name="message" id="exampleFormControlTextarea1" rows="3"><?php if(isset($message)) { echo $message; } ?></textarea>
	  </div>
	<div id="divbtn">	 
		<button id="button" type="submit" name="submit" >Envoyer</button>
	</div>
	</form>
 </div><br>

<h5>Où sommes nous?</h5>
<p>62, Avenue de la République, 70200 Lure.</p>
<div style="overflow:hidden;width: 700px;position: relative; margin: auto;"><iframe width="700" height="440" src="https://maps.google.com/maps?width=700&amp;height=440&amp;hl=en&amp;q=Flotey-Les-Lure+(A%C3%A9ro-club%20Flotey-Les-Lure)&amp;ie=UTF8&amp;t=&amp;z=11&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
<div style="position: absolute;width: 80%;bottom: 10px;left: 0;right: 0;margin-left: auto;margin-right: auto;color: #000;text-align: center;"><small style="line-height: 1.8;font-size: 2px;background: #fff;">Powered by <a href="https://embedgooglemaps.com/en/">embedgooglemaps FR</a> & <a href="http://botonmegusta.org/">botonmegusta.org/</a></small>
</div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
</div><br>

</div>

<?php include'body/footer.html'; ?>
</div>