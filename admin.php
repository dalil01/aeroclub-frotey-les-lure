<?php
include "connect/connect.php";

if(isset($_SESSION['admin']))	 
	{
	  $reqadmin = $bdd->prepare('SELECT * FROM membres WHERE id = ?');
      $reqadmin->execute(array($_SESSION['admin']));
      $admininfo = $reqadmin->fetch();

	}else{
		header ("Location: connexion.php");
	}

	$reservations = $bdd->query("SELECT * FROM reservations WHERE traitement = '0' ORDER BY date_besoin");
	$reservations_nbr = $reservations->rowCount();
	
	
	if($reservations_nbr < 2){
	    $affiche_reservation_nbr = "($reservations_nbr) Non-traitée";
	}else{
		$affiche_reservation_nbr = "($reservations_nbr) Non-traitées";
	}
	
	$resertrait = $bdd->query("SELECT * FROM reservations WHERE traitement = '1' ORDER BY date_besoin");
	$resertrait_nbr = $resertrait->rowCount();
	
	if($resertrait_nbr < 2){
	    $affiche_resertrait_nbr = "($resertrait_nbr) traitée";
	}else{
		$affiche_resertrait_nbr = "($resertrait_nbr) traitées";
	}

?>


<div class="container">
<?php include'body/header.php'; ?>

<div id="crps">
<h4><?= $admininfo['prenom']; ?> réservation</h4>
<h5 align=right><a href="nontrait.php"><?= $affiche_resertrait_nbr; ?></a></h5>
<h5 style="color: black;"><?= $affiche_reservation_nbr; ?></h5>
<br>

<?php 
while($affiche_reservation = $reservations->fetch()){
?>

<table class="table">

	<tr>
	  <?php
		$info_membre = $bdd->prepare('SELECT * FROM membres WHERE id = ?');
		$info_membre->execute(array($affiche_reservation['idmembre']));
		$infos_membre = $info_membre->fetch();
	  ?>
      <th scope="col">Nom</th>
    <td><?= $infos_membre['nom']; ?></td><br>
    </tr>
	<tr id="colchang">
      <th scope="col">Prenom</th>
	  <td><?= $infos_membre['prenom']; ?></td>
    </tr>
	<tr scope="col">
      <th scope="col">Mail</th>
	  <td><?= $infos_membre['mail']; ?></td>
    </tr>
 
    <tr id="colchang">
	<?php
		$info_reservation = $bdd->prepare('SELECT * FROM reservations WHERE idmembre = ?');
		$info_reservation->execute(array($affiche_reservation['idmembre']));
		$infos_reservation = $info_reservation->fetch();
	  ?>
      <th scope="col">Prestation</th>
	  <td><?= $infos_reservation['prestation']; ?></td>
    </tr>
    <tr>
      <th scope="col">Appareil</th>
	  <td><?= $infos_reservation['appareil']; ?><td>
    </tr>
    <tr id="colchang">
      <th scope="col">Date besoin</th>
	  <td><?= $infos_reservation['date_besoin']; ?></td>
    </tr>
	 <tr>
      <th scope="col">Durée</th>
	  <td><?= $infos_reservation['dure']; ?> h</td>
    </tr>
	 <tr id="colchang">
      <th scope="col">Disponibilité</th>
	  <td><?= $infos_reservation['dispo']; ?></td>
    </tr>
	<tr>
      <td></td>
	  <td>
	  <form method="Post">
			<div id="divbtn">	 
				<button id="button" type="submit" name="submit" >valider</button>
			</div>
		</form>
	  </td>
    </tr>
</table><br>
<?php } 

	$reser = $bdd->query("SELECT * FROM reservations WHERE traitement = '0'");
	$affiche_reser = $reser->fetch();

 
    if(isset($_POST['submit'])){
		$delete = $bdd->prepare("UPDATE reservations SET traitement = ? WHERE idreservation = ?");
		$delete->execute(array(1, $affiche_reser['idreservation']));
		header("Location: admin.php");
	}

?>

</div>

<?php include'body/footer.html'; ?>
</div>