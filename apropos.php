﻿<?php 
include "connect/connect.php";
?>
<div class="container">
<?php include'body/header.php'; ?>

<div id="crps">
<h4>A propos de nous</h4>


<div id="lastimgi">
	<p><img src="images/g.jpg"></p>
</div>

<div id="mysped">
<div class="container" >
  <div class="row">
    <div class="col-sm-5">
    	<p>	<img id="moyimg" src="images/rt.jpg" ></p>
   </div>
	
    <div class="col-sm-7">
	<h5 style="color: #000;">Entreprise</h5>
      <p>L’aéro-Club est une Association Loi 1901, agréée Jeunesse et Sports, adhérente à la FFULM 
		(Fédération Française d’ULM). <br> Association enregistrée n°04674 - - Agrément n° AS70986858. <br>
		Le siège social demeure 62, Avenue de la République, 70200 Lure 
		l’Aéro-Club assure la formation de pilotes et d’instructeurs.<br>
		Pour cela, il assure les prestations complémentaires suivantes :<br><br>
		- Maintenance des ULM moteur, structure…<br>
		- Montage des ULM et de tout instrument de navigation : Compas, radio, transpondeur…<br>
		- Un service de restauration rapide.<br>
		- Un hébergement en proposant trois studios et six chambres pour les élèves stagiaires.<br>
		- Location d’emplacement d’ULM dans des hangars pour les particuliers. 
		</p>
    </div>
  </div>
</div>
</div>

<h5>Notre équipe</h5>

<p>L’association est gérée par un conseil d’Administration. Dans la gestion courante, 7 personnes s’occupent de l’aéro-club.</p>

<table class="table">
<caption>Composition du bureau</caption>
  <thead>
    <tr>
      <th scope="col">Nom</th>
      <th scope="col">Prenom</th>
      <th scope="col">Poste</th>
    </tr>
  </thead>
  <tbody>
    <tr id="colchang">
      <td>Adit</td>
	  <td>Jacques</td>
	  <td>Président</td> 
    </tr>
    <tr>
      <td>Cleplatte</td>
	  <td>Igor</td>
	  <td>Mécanicien</td> 
    </tr>
    <tr id="colchang">
      <td>Saitout</td>
	  <td>Kevin</td>
	  <td>Chef pilote</td> 
    </tr>
	 <tr>
      <td>Laplume</td>
	  <td>Céline</td>
	  <td>Secrétaire</td> 
    </tr>
	 <tr id="colchang">
      <td>Monet</td>
	  <td>Claudine</td>
	  <td>Trésorière</td> 
    </tr>
	 <tr>
      <td>Volavu</td>
	  <td>Justine</td>
	  <td>Pilote ulm<br>Instructrice</td> 
    </tr>
	 <tr id="colchang">
      <td>Strument</td>
	  <td>Alain</td>
	  <td>Pilote ulm<br>Instructeur</td> 
    </tr>
  </tbody>
</table>


<div id="lastimgi">
	<p><img src="images/em.jpg"></p>
</div>

<h5>Nos partenaires</h5>

<table id="tabpart">
<tr>
<td>
	<a target="_blank" href="https://ffplum.fr">Fédération Française d'ULM</a>
</td>
</tr>
<td>
	<a target="_blank" href="https://www.bourgognefranchecomte.fr">Notre région</a>
</td>
<tr>
<td>
	<a target="_blank" href="https://aviation.meteo.fr" title="Site internet de Météo-France pour la préparation des vols.">Aéroweb</a>
</td>
</tr>
</table>



</div>

<?php include'body/footer.html'; ?>
</div>