﻿<?php 
include "connect/connect.php";
?>
<div class="container">
<?php include'body/header.php'; ?>

<div id="crps">
<h4>Découvrez l'ULM</h4>

<p>Vous avez toujours rêvé de voler ? <br>Mais vous n’avez jamais osé, ou vous avez cru que c’était inaccessible ? </p>
<p>L’ULM est la manière la plus simple de concrétiser votre rêve ! 
Vous y trouverez une très grande diversité de pratiques (6 classes) dans un cadre réglementaire cohérent basé sur la responsabilité et la liberté. Vous y trouverez de nombreuses structures et leurs instructeurs pour vous accueillir sur des aérodromes classiques ou sur les 800 bases ULM référencées. Vous y trouverez surtout un esprit au plus proche de l’aviation sportive et de loisir telle qu’elle existe depuis le début de l’histoire de l'aviation mais avec un technologie moderne et innovante qui offre des performances exceptionnelles pour découvrir la France d’en haut dans toute sa variété.
Soyez, avec les 16 000 licenciés de la Fédération, les acteurs de notre mouvement.
</p>

<h5 style="text-align: center;">Ils existent six classes d'ULM qui se differencient par leurs principes de vol</h5>

<div id="imagecat">
<p><img src="images/cat1.jpg"></p>
<p><img src="images/cat2.jpg"></p>
<p><img src="images/cat3.jpg"></p>
<p><img src="images/cat4.jpg"></p>
<p><img src="images/cat5.jpg"></p>
<p><img src="images/cat6.jpg"></p>
</div>

<?php include'body/footer.html'; ?>
</div>