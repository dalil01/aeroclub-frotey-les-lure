﻿<?php
include "connect/connect.php";

	if(isset($_POST['submit'])){
	
		$mail = htmlspecialchars(trim(strtolower($_POST['mail'])));
		$mdp = sha1($_POST['mdp']);
		
			if(($mail != "") && ($mdp != "")){
			
			   
			   $reqadmin = $bdd->prepare("SELECT * FROM membres WHERE mail = ? AND mdp = ? AND profil = ?");
	           $reqadmin->execute(array($mail, $mdp, 1));
	           $adminexist = $reqadmin->rowCount();
  
		if($adminexist == 1) {
			
			$admininfo = $reqadmin->fetch();
			  $_SESSION['admin'] = $admininfo['id'];
			   header("Location: admin.php");
						 exit;
  
		
		  }else {			  
            $error = "&#9888;  L’e-mail ou le mot de passe ne correspondent à aucun compte !"; 
	      }
		}else{
			$error = "&#9888; Tous les champs doivent être complétés !";
		}
		
		if(($mail != "") && ($mdp != "")){
			
			   $requser = $bdd->prepare("SELECT * FROM membres WHERE mail = ? AND mdp = ? AND profil = ?");
	           $requser->execute(array($mail, $mdp, 0));
	           $userexist = $requser->rowCount();
			
  
  
	      if($userexist == 1) {
			  
			  $userinfo = $requser->fetch();
			  $_SESSION['membre'] = $userinfo['id'];
			   header("Location: profil.php");
						 exit;
		 
		
		  }else {			  
            $error = "&#9888;  L’e-mail ou le mot de passe ne correspondent à aucun compte !"; 
	      }
		}else{
			$error = "&#9888; Tous les champs doivent être complétés !";
		}
	}
	

?>

<div class="container">
<?php include'body/header.php'; ?>

<div id="crps">
<h4>Connexion</h4>

<div id="conlist">
<p id="error"><?php echo (isset($error)) ? $error : ''; ?></p>
	<form method="POST">
	  <div class="form-group">
		<label for="exampleFormControlSelect1">Adresse e-mail</label>
		<input type="email" class="form-control" id="exampleFormControlInput1" name="mail">
	  </div>
	  <div class="form-group">
		<label for="exampleFormControlSelect2">Mot de passe</label>
		<input type="password" class="form-control" id="exampleFormControlInput1" name="mdp">
	  </div>
	<div id="divbtn">	 
		<button id="button" type="submit" name="submit" >Connexion</button>
	</div>
	</form>
 </div>

</div>

<?php include'body/footer.html'; ?>
</div>