﻿<?php
include "connect/connect.php";
	
	if(isset($_POST['submit'])){
		$nom = htmlspecialchars(ucfirst(trim($_POST['nom'])));
		$prenom = htmlspecialchars(ucfirst(trim($_POST['prenom'])));
		$mail = htmlspecialchars(trim(strtolower($_POST['mail'])));
		$mdp = sha1($_POST['mdp']);
		$mdp2 = sha1($_POST['mdp2']);
		
		if(($nom != "") && ($prenom != "") && ($mail != "") && ($mdp != "") && ($mdp2 != "")){
			$verifmail = $bdd->prepare("SELECT mail FROM membres WHERE mail = ? ");
			$verifmail->execute(array($mail));
			$verifmail = $verifmail->rowCount();
				
		if($verifmail == 0){
		if($mdp == $mdp2){
			
			
			   $insert = $bdd->prepare("INSERT INTO membres(nom, prenom, mail, mdp, profil) VALUES(?, ?, ?, ?, ?)");
			   $insert->execute(array($nom, $prenom, $mail, $mdp, 0));			 
			   $error = "Votre compte a bien été créé !";
				
		}else{
			$error = "&#9888; Vos mots de passes ne correspondent pas. Veuillez réessayer !";
		}
		}else{
			$error = "&#9888; Adresse e-mail déjà utilisée";
		}
		}else{
			$error = "&#9888; Tous les champs doivent être complétés !";
		}
	}
	

?>
<div class="container">
<?php include'body/header.php'; ?>

<div id="crps">
<h4>Inscription</h4>

<div id="inslist">
<p id="error"><?php echo (isset($error)) ? $error : ''; ?></p>
	<form method="POST">
	  <div class="form-group">
		<label for="exampleFormControlInput1">Votre nom</label>
		<input type="text" class="form-control" id="exampleFormControlInput1" name="nom"
		value="<?php if(isset($nom)) { echo $nom; } ?>">
	  </div>
	  <div class="form-group">
		<label for="exampleFormControlInput1">Votre prenom</label>
		<input type="text" class="form-control" id="exampleFormControlInput1" name="prenom"
		value="<?php if(isset($prenom)) { echo $prenom; } ?>">
	  </div>
	  <div class="form-group">
		<label for="exampleFormControlSelect1">Adresse e-mail</label>
		<input type="email" class="form-control" id="exampleFormControlInput1" name="mail"
		value="<?php if(isset($mail)) { echo $mail; } ?>">
	  </div>
	  <div class="form-group">
		<label for="exampleFormControlSelect2">Mot de passe</label>
		<input type="password" class="form-control" id="exampleFormControlInput1" name="mdp">
	  </div>
	   <div class="form-group">
		<label for="exampleFormControlSelect2">Confirmer votre passe</label>
		<input type="password" class="form-control" id="exampleFormControlInput1" name="mdp2">
	  </div>
	<div id="divbtn">	 
		<button id="button" type="submit" name="submit" >Inscription</button>
	</div>
	</form>
 </div>

</div>

<?php include'body/footer.html'; ?>
</div>