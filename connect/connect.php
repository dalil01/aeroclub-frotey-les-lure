<?php

session_start();
$dbhost = 'localhost';
$dbname = 'aeroclub';
$dbuser = 'root';
$dbpswd = '';

try{
	$bdd = new PDO('mysql:host='.$dbhost.';dbname='.$dbname,$dbuser,$dbpswd, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
}catch(PDOexception $e){
	die("Une erreur est survenue lors de la connexion a la base de données");
}

function isLogged_admin(){
	if(isset($_SESSION['admin'])){
		$logged_admin = 1;
	}else{
		$logged_admin = 0;
	}
	return $logged_admin;
}

function isLogged_membre(){
	if(isset($_SESSION['membre'])){
		$logged_membre = 1;
	}else{
		$logged_membre = 0;
	}
	return $logged_membre;
}