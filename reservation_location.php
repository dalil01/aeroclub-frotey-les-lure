<?php 
include "connect/connect.php";

if(isset($_SESSION['membre']))	 
	{
	  $requser = $bdd->prepare('SELECT * FROM membres WHERE id = ?');
      $requser->execute(array($_SESSION['membre']));
      $userinfo = $requser->fetch();

	}else{
		header ("Location: connexion.php");
	}

if(isset($_POST['submit'])){
		
		$appareil = htmlspecialchars($_POST['appareil']);
		$date = htmlspecialchars($_POST['date']);
		$dure = htmlspecialchars($_POST['dure']);
		$dispo = htmlspecialchars($_POST['dispo']);
		
		if(!empty($appareil) and (!empty($date)) and (!empty($dure)) and (!empty($dispo))){
		
			$insertcours = $bdd->prepare("INSERT INTO reservations VALUES(?,?,?,?,?,?,?,NOW(),?)");
			$insertcours->execute(array(NULL,$userinfo['id'], $appareil, "location", $date, $dure, $dispo,0));
			
			
			$error = "Votre réservation a bien été enregistrée, vous recevrez une confirmation par mail !";
			
		}else{
			$error = "&#9888; Tous les champs doivent être complétés !";
		}
		
		
	}

?>

<div class="container">
<?php include'body/header.php'; ?>

<div id="crps">

<h4>Location d'appareil</h4>

<table class="table">
  <thead>
	 <tr>
      <th scope="col">Appareil</th>
      <th scope="col">Heure de vol</th>
      <th scope="col">Forfait 40 heures</th>
	  <th scope="col">Forfait annuel illimité</th>
    </tr>
  </thead>
  <tbody>
    <tr id="colchang">
      <td>Air Creation Trek HKS 700E FUN450</td>
	  <td>80.00 €</td>
	  <td>2500.00 €</td> 
	  <td>5200.00 €</td> 
    </tr>
    <tr>
      <td>MAGNI M22 - Rotax 914</td>
	  <td>120.00 €</td>
	  <td>4000.00 €</td> 
	  <td>8100.00 €</td>
    </tr>
    <tr id="colchang">
      <td>Croses Criquet léger LC 12</td>
	  <td>90.00 €</td>
	  <td>2900.00 €</td> 
	  <td>6000.00 €</td>
    </tr>
	<tr><td></td></tr>
  </tbody>
</table>	


<div id="conlist">
<p id="error"><?php echo (isset($error)) ? $error : ''; ?></p>
	<form method="POST">
	  <div class="form-group">
		<label for="exampleFormControlSelect1">Appareil</label><br>
		<select class="form-control" id="exampleFormControlInput1" name="appareil">
			<option value="Pendulaire">Pendulaire</option>
			<option value="Autogire">Autogire</option>
			<option value="Multiaxes">Multiaxes</option>
		</select>
	  </div>
	  <div class="form-group">
		<label for="exampleFormControlSelect2">Date</label>
		<input type="date" class="form-control" id="exampleFormControlInput1" name="date">
	  </div>
	   <div class="form-group">
		<label for="exampleFormControlSelect2">Heure de vol</label>
		<input type="number" class="form-control" id="exampleFormControlInput1" name="dure">
	  </div>
	   <div class="form-group">
		<label for="exampleFormControlSelect2">Disponibilité</label>
		<input type="text" placeholder="Ex : Entre 13h et 15h" class="form-control" id="exampleFormControlInput1" name="dispo">
	  </div>
	  
	<div id="divbtn">	 
		<button id="button" type="submit" name="submit" >valider</button>
	</div>
	</form>
 </div>	
 </div>	

<?php include'body/footer.html'; ?>
</div>

