﻿<?php 
include "connect/connect.php";
?>
<div class="container">
<?php include'body/header.php'; ?>

<div id="crps">
<h4>Qui peut voler ?</h4>

<p> Afin de pouvoir voler, un brevet et une license de pilote d'ULM est obligatoire. La réglementation stipule que pour obtenir le brevet et la licence de pilote d'ULM, le candidat doit remplir les conditions suivantes :
<br>- Être âgé de 15 ans révolus.<br>
- Avoir satisfait à un examen au sol (QCM)  commun aux 6 classes, et un questionnaire spécifique à la classe pratiquée.
<br>- Avoir ensuite reçu une autorisation de vol seul à bord par un instructeur habilité.
</p>

<div id="lienpdf">
<h6>Pour plus d'informations, vous avez la possibilité de
 consulter notre documentation téléchargeable en cliquant sur ces liens.</h6>
 </div>

<table id="tabpart">
<tr>
<td>
	<a target="_blank" href="decouverte.pdf">Découvrir l'ULM</a>
</td>
</tr>
<td>
	<a target="_blank" href="secure.pdf">Sécurité du pilote ULM</a>
</td>
</table>

</div>
<?php include'body/footer.html'; ?>
</div>