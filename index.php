<?php 
include "connect/connect.php";
?>	
<div  class="container">
<?php include'body/header.php'; ?>

<div id="crps">

<h4>L'aéro-Club de Frotey-Lès-Lure</h4>
<p id="addr">62, Avenue de la République, 70200 Lure</p>
<p>L'association vous offre la chance de pratiquer l'aviation légère dans un
 environnement exceptionnel : </br> la Haute-Saône et ses paysages variés, collines, 
 vallées de la Saône et de l'Ognon, Vosges-Saônoises et plateau des Mille Étangs...</p>


<div class="container" id="slpad">

  <div class="mySlides">
    <div class="numbertext">1 / 6</div>
      <img src="images/az.jpg" id="imgacc" >
  </div>

  <div class="mySlides">
    <div class="numbertext">2 / 6</div>
      <img src="images/d.jpg" id="imgacc">
  </div>

  <div class="mySlides">
    <div class="numbertext">3 / 6</div>
      <img src="images/h.jpg" id="imgacc">
  </div>

  <div class="mySlides">
    <div class="numbertext">4 / 6</div>
      <img src="images/pp.jpg" id="imgacc">
  </div>

  <div class="mySlides">
    <div class="numbertext">5 / 6</div>
      <img src="images/ac.png" id="imgacc">
  </div>

  <div class="mySlides">
    <div class="numbertext">6 / 6</div>
      <img src="images/j.jpg" id="imgacc">
  </div>

  <div class="caption-container">
    <p id="caption"></p>
  </div>

  <div class="row">
    <div class="column">
      <img class="demo cursor" src="images/az.jpg" id="imgptit" onclick="currentSlide(1)" alt="Multiaxes">
    </div>
    <div class="column">
      <img class="demo cursor" src="images/d.jpg" id="imgptit" onclick="currentSlide(2)" alt="Hélico">
    </div>
    <div class="column">
      <img class="demo cursor" src="images/h.jpg" id="imgptit" onclick="currentSlide(3)" alt="Autogire">
    </div>
    <div class="column">
      <img class="demo cursor" src="images/pp.jpg" id="imgptit" onclick="currentSlide(4)" alt="Paramoteur">
    </div>
    <div class="column">
      <img class="demo cursor" src="images/ac.png" id="imgptit" onclick="currentSlide(5)" alt="Pendulaire">
    </div>
    <div class="column">
      <img class="demo cursor" src="images/j.jpg" id="imgptit" onclick="currentSlide(6)" alt="Aerostat">
    </div>
  </div>
</div>


<h5>Sur le site, nous disposons de :</h5>
<div id="msli">
	<p>- Une surface totale de 45 hectares dégagés. 
	</br>- Deux pistes en X de 800 et 450 m.
	</br>- Deux pistes en X de 800 et 450 m.
	</br>- Un hydrosurface de 8 hectares.
	</br>- Trois hangars avec surface couverte de 4300 m².
	</br>- Locaux administratifs informatisés. 
	</br>- Salle de cours multimédia avec simulateur de vol. 
	</br>- Un atelier d'entretien et de réparation. 
	</br>- Des capacités d'accueil et de restauration sur place. 
	</br>- Connexion WIFI gratuit sur toute la base</p>			
</div>

<div id="divaco">
<button class="accordion">Positionnement des pistes et des bâtiments de la base </button>

<div class="panel">
  <img src="images/rr.png" >
</div>
</div>

<h5>Les moyens pédagogiques</h5>

<div id="mysped">
<div class="container" >
  <div class="row">
    <div class="col-sm-5">
      <h5 style="color: #000; margin-top: -15px;">En vol :</h5>
      <h6 style="color: #000;">huit appareils...</h6>
      <p>- Trois pendulaires</p>
	  <p>- Quatre multiaxes</p>
	  <p>- Un autogire</p>
	  <br>
	   <h5 style="color: #000;">Au sol :</h5>
	  <p>- Une salle OPS et deux salles de cours pour la théorie</p>
		<p>- Vidéo-projection pendant les leçons</p>
		<p>- Un ordinateur à votre disposition pour travailler les QCMs examen.</p>
    	<br><p><img id="moyimg" src="images/tr.jpg" ></p>
		<p><img id="moyimg" src="images/fr.jpg" ></p>
   </div>
	
    <div class="col-sm-7">
      <p><img id="moyimg" src="images/auto.jpg" ></p><br>
      <p><img id="moyimg" src="images/ze.png"></p><br>
      <p><img  id="moyimg" src="images/b.jpg"></p>
    </div>
  </div>
</div>
</div>

<div id="divaco">
<button class="accordion">Salle de cours</button>

<div class="panel">
  <img src="images/sal.png" >
</div>
</div>

<div id="lastimgi">
	<p><img src="images/i.jpg"></p>
</div>

<h5>Hébergement</h5>

<div>
	<p>Les appartements sont équipés d’une kitchenette, d’une salle de bain (lavabo, douche, W.C.), télévision, lit en 140
    Draps fournis.<br>Électricité en plus (compteur individuel). Caution 100 euros. </p>
</div>

<div id="divaco">
<button class="accordion">Appartement</button>
<div class="panel">
  <img src="images/app.png" >
</div>
</div>

<div>
<p>Les chambres sont équipées d’une salle de bain (lavabo, douche, W.C.), télévision, lit en 140
Draps fournis.<br>Caution 100 euros. 
</p>
</div>

<div id="divaco">
<button class="accordion">Chambre</button>
<div class="panel">
  <img src="images/chmb.png" >
</div>
</div>

</div>

<script>
var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}


var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    /* Toggle between adding and removing the "active" class,
    to highlight the button that controls the panel */
    this.classList.toggle("active");

    /* Toggle between hiding and showing the active panel */
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>

<?php include'body/footer.html'; ?>
 </div>