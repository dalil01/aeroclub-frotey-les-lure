<?php
include "connect/connect.php";

if(isset($_SESSION['membre']))	 
	{
	  $requser = $bdd->prepare('SELECT * FROM membres WHERE id = ?');
      $requser->execute(array($_SESSION['membre']));
      $userinfo = $requser->fetch();

	}else{
		header ("Location: connexion.php");
	}


	if(isset($_POST['submit'])){
		$prestation = $_POST['prestation'];
		
		if($prestation == "cours"){
			header("Location: reservation_cours.php");
		}
		if($prestation == "bdl"){
			header("Location: reservation_bdl.php");
		}
		if($prestation == "location"){
			header("Location: reservation_location.php");
		}
		
	}
	

?>

<div class="container">
<?php include'body/header.php'; ?>

<div id="crps">
<h5><?= $userinfo['prenom']; ?> <?= $userinfo['nom']; ?></h5>
<h4>Réservation</h4>

<form method="post">
<table align="center" style="text-align: center" >
<tr>
<th>
<div class="form-check">
  <input class="form-check-input" type="radio" name="prestation" id="exampleRadios1" value="cours" checked>
  <label class="form-check-label" for="exampleRadios1">
    Cours
  </label>
</div>
</th>
<td width=10></td>
<th>
<div class="form-check">
  <input class="form-check-input" type="radio" name="prestation" id="exampleRadios1" value="bdl" >
  <label class="form-check-label" for="exampleRadios1">
    Baptême de l'air
  </label>
</div>
</th>
<td width=10></td>
<th>

<div class="form-check">
  <input class="form-check-input" type="radio" name="prestation" id="exampleRadios1" value="location" >
  <label class="form-check-label" for="exampleRadios1">
    location
  </label>
</div>
</th>
</tr>
<tr><td height=18></td></tr>
<tr>
<th></th>
<td width=10></td>
<th>
<div class="form-check">
  <input type="submit"  name="submit" value="Valider">
</div>
</th>
<td width=10></td>
<th></th>
</tr>
</table>
</form>


</div>

<?php include'body/footer.html'; ?>
</div>