<?php

 if((isLogged_membre() == 0) and (isLogged_admin() == 0)){
 
?>
<!doctype html>

<html lang="fr">
<head>
	<meta charset="UTF-8">
	<link rel="shortcut icon" type="image/x-icon" href="images/logo.png" />
	<title>Aéro-club</title>
	<meta content="Site de l'aéroclub de Frotey-lés-lure" name="description"/>
	<meta content="ACF2L, aeroclub, frotey-lès-lure, froteyleslureaeroclub" name="keywords"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/lestyle.css"/>
</head>

<div id="header">

<table id="tablone">
<tr>
<td>
	<a href="index.php"><img src="images/logo.png" width="150"></a>
</td>

<td  id="headright">
	<div id="name"><h3>ACF2L</h3> <p>70200 Lure</p></div>
</td>
</tr>
</table>

<nav class="navbar navbar-expand-lg navbar-light bg-light">

  <a class="navbar-brand" id="accel" href="index.php">&nbsp;&nbsp;&nbsp;Accueil</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-md-auto">
      <li class="nav-item" id="navlk">
        <a class="nav-link" href="categoriesulm.php">Les catégories d'ULM</a>
      </li>
      <li class="nav-item" id="navlk">
        <a class="nav-link" href="quipeutvoler.php">Qui peut voler ?</a>
      </li>
	   <li class="nav-item" id="navlk">
        <a class="nav-link" href="nostarifs.php">Nos tarifs</a>
      </li>
	  <li class="nav-item" id="navlk">
        <a class="nav-link" href="webcam.php">Webcam</a>
      </li>
	  <li class="nav-item" id="navlk">
        <a class="nav-link" href="meteo.php">Météo</a>
      </li>
    </ul>
  </div>
</nav>

<table id="tabinscon" >
<tr>
	<td>	
		<a href="inscription.php" id="ins">Inscription</a>
	</td>
	<td>
	    <i class="fa fa-plane" id="avon"></i>
	</td>
	<td>
		<a href="connexion.php"  href="" id="con">Connexion</a>
	</td>
</tr>
</table>


</div>
<?php 
 }
 if(isLogged_membre() == 1){
?> 
<!doctype html>

<html lang="fr">
<head>
	<meta charset="UTF-8">
	<link rel="shortcut icon" type="image/x-icon" href="images/logo.png" />
	<title>Aéro-club</title>
	<meta content="Site de l'aéroclub de Frotey-lés-lure" name="description"/>
	<meta content="ACF2L, aeroclub, frotey-lès-lure, froteyleslureaeroclub" name="keywords"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/lestyle.css"/>
</head>

<div id="header">

<table id="tablone">
<tr>
<td>
	<a href="index.php"><img src="images/logo.png" width="150"></a>
</td>

<td  id="headright">
	<div id="name"><h3>ACF2L</h3> <p>70200 Lure</p></div>
</td>
</tr>
</table>

<nav class="navbar navbar-expand-lg navbar-light bg-light">

  <a class="navbar-brand" id="accel" href="profil.php">&nbsp;&nbsp;&nbsp;Profil</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-md-auto">
	  <li class="nav-item" id="navlk">
        <a class="nav-link" href="deconnexion.php">Se déconnecter</a>
      </li>
    </ul>
  </div>
</nav>

</div>
<?php 
 }
 if(isLogged_admin() == 1){
?> 
<!doctype html>

<html lang="fr">
<head>
	<meta charset="UTF-8">
	<link rel="shortcut icon" type="image/x-icon" href="images/logo.png" />
	<title>Aéro-club</title>
	<meta content="Site de l'aéroclub de Frotey-lés-lure" name="description"/>
	<meta content="ACF2L, aeroclub, frotey-lès-lure, froteyleslureaeroclub" name="keywords"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/lestyle.css"/>
</head>

<div id="header">

<table id="tablone">
<tr>
<td>
	<a href="index.php"><img src="images/logo.png" width="150"></a>
</td>

<td  id="headright">
	<div id="name"><h3>ACF2L</h3> <p>70200 Lure</p></div>
</td>
</tr>
</table>

<nav class="navbar navbar-expand-lg navbar-light bg-light">

  <a class="navbar-brand" id="accel" href="admin.php">&nbsp;&nbsp;&nbsp;Profil</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-md-auto">
	  <li class="nav-item" id="navlk">
        <a class="nav-link" href="deconnexion.php">Se déconnecter</a>
      </li>
    </ul>
  </div>
</nav>

</div>
<?php
 }
?> 